import axios from "axios"
import { WS_COMMENTS, WS_POSTS } from "../config/ServiceConfig"

const findAll = async () => {
    let result = await axios.get(WS_POSTS)
    return result.data
}

const findById = async (postId) => {
    let result = await axios.get(`${WS_POSTS}/${postId}`)
    return result.data
}

const getComments = async (postId) => {
    let result = await  axios.get(`${WS_POSTS}/${postId}/${WS_COMMENTS}`)
    return result.data
}

const postService = { findAll, findById, getComments }

export default postService