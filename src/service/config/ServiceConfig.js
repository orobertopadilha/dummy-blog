const BASE_URL = "https://jsonplaceholder.typicode.com"

export const WS_POSTS = `${BASE_URL}/posts`
export const WS_COMMENTS = `comments`