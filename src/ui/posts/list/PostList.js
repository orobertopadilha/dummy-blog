import { useCallback, useEffect, useState } from "react"
import { Link } from "react-router-dom"
import postService from "../../../service/post/PostService"

const PostList = () => {

    const [posts, setPosts] = useState([])

    const loadPosts = useCallback(async () => {
        let result = await postService.findAll()
        setPosts(result)    
    }, [])

    useEffect(() => loadPosts(), [loadPosts])

    return (
        <div>
            {
                posts.map(p => 
                    <div key={p.id} style={{marginBottom: '36px'}}>
                        <h3 style={{margin: 0}}>
                            {p.title}
                        </h3>
                        <p style={{margin: 0}}>
                            {p.body} 
                            
                            <br />
                            <Link to={`/post-details/${p.id}`}>ver mais</Link>
                        </p>
                    </div>
                )
            }
        </div>
    )
}

export default PostList