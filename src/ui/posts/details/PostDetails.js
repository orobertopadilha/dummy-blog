import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import postService from "../../../service/post/PostService"

const PostDetails = () => {
    const { postId } = useParams()

    const [post, setPost] = useState()
    const [comments, setComments] = useState([])

    useEffect(() => {

        const loadData = async () => {
            let resultPost = await postService.findById(postId)
            let resultComment = await postService.getComments(postId)

            setPost(resultPost)
            setComments(resultComment)
        }

        loadData()
    }, [postId])

    return (
        <>
            {post &&

                <div>
                    <h3>{post.title}</h3>
                    <p>{post.body}</p>

                    <br />
                    <b>Comentários</b>

                    {
                        comments.map(c =>
                            <p key={c.id}>
                                <b>{c.email}</b>
                                <br />
                                {c.body}
                            </p>
                        )
                    }
                </div>
            }
        </>
    )
}

export default PostDetails