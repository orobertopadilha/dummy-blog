import { Route, Switch } from "react-router-dom"
import PostDetails from "../posts/details/PostDetails"
import PostList from "../posts/list/PostList"

const AppRouter = () => {
    return(
        <Switch>
            <Route path="/" exact>
                <PostList />
            </Route>
            <Route path="/posts">
                <PostList />
            </Route>
            <Route path="/post-details/:postId">
                <PostDetails />
            </Route>
        </Switch>
    )
}

export default AppRouter