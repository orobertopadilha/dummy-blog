import { BrowserRouter } from "react-router-dom"
import AppHeader from "./ui/header/AppHeader"
import AppRouter from "./ui/router/AppRouter"

const App = () => {

    return (
        <BrowserRouter>
            <AppHeader />
            <AppRouter />
        </BrowserRouter>
    )
}

export default App
